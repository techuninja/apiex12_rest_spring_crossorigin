package com.techu.apiex12_rest_spring_crossorigin.holamundo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class HolaMundoController {

    @GetMapping("/saludo")
    public String saludar(){
        return "Hola Tech U!";
    }
}
