package com.techu.apiex12_rest_spring_crossorigin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apiex12RestSpringCrossoriginApplication {

    public static void main(String[] args) {
        SpringApplication.run(Apiex12RestSpringCrossoriginApplication.class, args);
    }

}
